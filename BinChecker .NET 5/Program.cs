﻿using ConsoleTables;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.IO;

namespace BinChecker_.NET_5
{
   
    public class Content
    {
        public string BIN { get; set; }
        public string Vendor { get; set; }
        public string Type { get; set; }
        public string Level { get; set; }
        public string Bank { get; set; }
        public string Country { get; set; }
    }

    internal class Program
    {
        static async Task Main(string[] args)
        {

            Start:
            string bin;
            Console.WriteLine("Enter a BIN/IIN Code: ");
            Console.WriteLine();
            bin = Console.ReadLine();

            string url = "https://bin-check-dr4g.herokuapp.com/api/";
            var client = new RestClient(url + bin);
            var request = new RestRequest();
            var response = await client.GetAsync(request);

            JObject obj = JObject.Parse(response.Content);

            JObject data = (JObject)obj["data"];

            Content content = JsonConvert.DeserializeObject<Content>(data.ToString());

            var table = new ConsoleTable("BIN", "Vendor", "Type", "Level", "Bank", "Country");

            table.AddRow(content.BIN, content.Vendor, content.Type, content.Level, content.Bank, content.Country);
            table.Write();

            string answer;
            Console.WriteLine("Dou you want to check another code? (y/n) ");
            Console.WriteLine();
            answer = Console.ReadLine();

            if (answer == "y")
            {
                goto Start;
            }

            
        }
    }
}
